# Disclaimer
### This project was created by following the tutorial below:
https://youtube.com/watch?v=zqV7NIFGDrQ

### Original Author's GitHub:
https://github.com/scps960740/React-crash-course-2021-bruceFE

### Purpose
Purpose of this project was to learn basics of React.js and React Hooks. Code in this repository is copied from the original Author's GitHub/written alongside watching the tutorial, and is done so for educational purposes.

