import { useState, useEffect, useRef } from 'react'
import { API_GET_DATA } from '../../global/constants'

// Import components here
import Edit from './components/Edit'
import List from './components/List'
import './index.css'

/* useEffect:

useEffect(() => {
  // whenever 'data' changes, this function will execute
}, [data])

*/

// asynchronous function for fetching data from API
async function fetchData(setData) {
  // wait for fetched data via API call
  const res = await fetch(API_GET_DATA)
  // convert res to json
  const { data } = await res.json()
  // set the data (line 31)
  setData(data)
}

// asynchronous function for setting data to backend
async function fetchSetData(data) {
  const res = await fetch(API_GET_DATA, {
    method: "PUT",
    headers: {
      'Content-type': 'application/json'
    },
    body: JSON.stringify({data})
  })
}

const Home = () => {
  // data will hold the list
  // setData will allow us to change the list
  const [data, setData] = useState([]);
  const submittingStatus = useRef(false);

  // when application starts, this will run once whether or
  // not data has any changes. Data is initially empty so it
  // will clear out the database.
  // Solution: useRef whose value persist for its full lifetime

  // send items in data to backend
  // if not submittingStatus then return
  // else send the data to backend then make
  //    submittingStatus to false
  useEffect(() => {
    if(!submittingStatus.current)   return;
    fetchSetData(data)
    .then(data => submittingStatus.current = false)
  }, [data])

  // fetch API to get data from backend
  useEffect(() => {
    fetchData(setData)
  }, [])

  // listData is a parameter sent to List component
  return <div className="app">
    <Edit add={setData} submittingStatus={submittingStatus}/>
    <List listData={data} deleteData={setData} submittingStatus={submittingStatus}/>
  </div>
}

export default Home