import { useState } from 'react';
import { v4 } from 'uuid';

const Edit = ({ add, submittingStatus}) => {

  // create 3 state variables for each input field
  // Note
  const [note, setNote] = useState("")
  function noteChange(e){
    // callback function for when input field
    // of note is changed
    setNote(e.target.value)
  }

  // Date
  const [date, setDate] = useState("")
  function dateChange(e){
    setDate(e.target.value)
  }

  // Time
  const [time, setTime] = useState("")
  function timeChange(e){
    setTime(e.target.value)
  }

  function addItem(){
    // add function is setData function from Home
    // append new data to previous data
    submittingStatus.current = true
    add(function(prev){
      return [{
        id: v4(),
        note,
        date,
        time
      }, ...prev]
    })
  }

  return <div>
    <h1>備忘錄</h1>
    <p>记事</p>
    <input type="text" value={note} onChange={noteChange}></input>
    <p>日期</p>
    <input type="date" value={date} onChange={dateChange}></input>
    <p>時間</p>
    <input type="time" value={time} onChange={timeChange}></input>
    <button onClick={addItem} className="add">新增</button>
  </div>
}

export default Edit